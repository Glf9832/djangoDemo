# -*- coding:utf-8 -*-

from __future__ import absolute_import
import os
import sys
from .celery_app import celery_app
from datetime import datetime
from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync
from common.system_info import get_sys_info
import json
from schedule.celery_result import CeleryResult

from celery.signals import task_success,task_failure,task_retry,task_prerun,task_postrun
from celery.contrib import rdb
import datetime
from common.log import traverse_check_dir


BASE_DIR=os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(BASE_DIR)

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "djangopro.settings")

@celery_app.task
def channels_test_task():
    channel_layer = get_channel_layer()
    t,cpus,memory = get_sys_info()
    sysinfo = {
        "time":t,
        "cpu":cpus,
        "memory":memory,
    }
    async_to_sync(channel_layer.group_send)("chat_test_group",{
                'type': 'chat_message',
                'message': json.dumps(sysinfo)
            })

    return sysinfo

@celery_app.task
def log_check_task(kwargs):
    result = traverse_check_dir(**kwargs)
    return {
        'result':result,
        'check_time':datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    }

@celery_app.task(autoretry_for=(Exception,),retry_kwargs={'max_retries': 5})
def test_task(params):
    a = int('a')
    return None

@task_prerun.connect
def task_prerun_signals(sender=None,**kwargs):
    params = {
        'task_id':sender.request.id,
        'task_started':datetime.datetime.now()
    }
    CeleryResult.save_started(**params)

@task_postrun.connect
def task_postrun_signals(sender=None,**kwargs):
    params = {
        'task_id':sender.request.id,
        'task_end':datetime.datetime.now()
    }
    CeleryResult.save_end(**params)

@task_success.connect
def task_success_signals(sender=None,result=None,args=None,exception=None,**kwargs):
    params = {
        'task_id':sender.request.id,
        'task_name':sender.request.task,
        'task_status':"success",
        'task_args':str(args),
        'task_result':result,
        'task_succeeded':datetime.datetime.now(),
        'task_failure':None,
        'task_except':str(exception),
    }
    CeleryResult.save_task_result(**params)

@task_failure.connect
def task_failure_signals(sender=None,result=None,args=None,exception=None,**kwargs):
    params = {
        'task_id':sender.request.id,
        'task_name':sender.request.task,
        'task_status':"failure",
        'task_args': str(args),
        'task_result':result,
        'task_succeeded':None,
        'task_failure': datetime.datetime.now(),
        'task_except':str(exception),
    }
    CeleryResult.save_task_result(**params)

@task_retry.connect
def task_retry_signals(sender=None,**kwargs):
    # print('task retry')
    # print('task_id:{}'.format(sender.request.id))
    # print('task:{}'.format(sender.request.task))
    pass

