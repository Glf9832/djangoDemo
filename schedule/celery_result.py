from common.db_engine import get_session
from schedule.models_celery import CeleryTaskResult
from common.log import log_setup_db,get_logger

class CeleryResult(object):
    @staticmethod
    def save_started(**kwargs):
        session = get_session()
        task_id = kwargs.get("task_id")
        task_started = kwargs.get("task_started")
        task = session.query(CeleryTaskResult).filter(CeleryTaskResult.id_task_uuid==task_id).first()
        try:
            if task is None:
                ctr = CeleryTaskResult(id_task_uuid=task_id,task_started=task_started)
                session.add(ctr)
            else:
                session.query(CeleryTaskResult).filter(CeleryTaskResult.id_task_uuid==task_id).update({'task_started':task_started})
            session.commit()
        except Exception as e:
            session.rollback()
            global logger
            log_setup_db()
            logger = get_logger()
            logger.error('save_started error.{}'.format(e))
        finally:
            session.close()

    @staticmethod
    def save_end(**kwargs):
        session = get_session()
        task_id = kwargs.get("task_id")
        task_end = kwargs.get("task_end")
        task = session.query(CeleryTaskResult).filter(CeleryTaskResult.id_task_uuid==task_id).first()
        try:
            if task is None:
                ctr = CeleryTaskResult(id_task_uuid=task_id,task_started=task_end)
                session.add(ctr)
            else:
                session.query(CeleryTaskResult).filter(CeleryTaskResult.id_task_uuid==task_id).update({'task_end':task_end})
            session.commit()
        except Exception as e:
            session.rollback()
            global logger
            log_setup_db()
            logger = get_logger()
            logger.error('save_started error.{}'.format(e))
        finally:
            session.close()

    @staticmethod
    def save_task_result(**kwargs):
        session = get_session()
        task_id = kwargs.get("task_id")
        task_name = kwargs.get("task_name")
        task_status = kwargs.get("task_status")
        task_args = kwargs.get("task_args")
        task_result = kwargs.get("task_result",'')
        task_succeeded = kwargs.get("task_succeeded")
        task_failure = kwargs.get("task_failure")
        task_except = kwargs.get("task_except",'')

        task = session.query(CeleryTaskResult).filter(CeleryTaskResult.id_task_uuid==task_id).first()
        try:
            if task is None:
                ctr = CeleryTaskResult(id_task_uuid=task_id,
                                       task_name=task_name,
                                       status=task_status,
                                       task_args=task_args,
                                       task_result=task_result,
                                       task_succeeded=task_succeeded,
                                       task_failure= task_failure,
                                       task_except=task_except,
                                       )
                session.add(ctr)
            else:
                session.query(CeleryTaskResult).filter(CeleryTaskResult.id_task_uuid==task_id).update({
                    'task_name':task_name,
                    'status': task_status,
                    'task_args':task_args,
                    'task_result': task_result,
                    'task_succeeded': task_succeeded,
                    'task_failure':task_failure,
                    'task_except': task_except,
                })
            session.commit()
        except Exception as e:
            session.rollback()
            global logger
            log_setup_db()
            logger = get_logger()
            logger.info('save_started error.{}'.format(e))
        finally:
            session.close()

if __name__ == '__main__':
    pass