import json
from datetime import datetime

def task_started_msg(task_id):
    return json.dumps({
        'status': 'start successful',
        'jobid':task_id,
        'time':datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    })

def task_started_error_msg(des):
    return json.dumps({
        'state':'start failed',
        'error':str(des)
    })
