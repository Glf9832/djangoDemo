
debug = False

address = '0.0.0.0'

port = 5555     # 默认5555

auto_refresh = True

broker_api = 'amqp://greene:123456@10.110.139.128:5672/'

max_tasks = 10000     # 内存中保留最大的task数目 默认10000

persistent = True

db = '/home/glf/djangoDemo/flower/fdb'

# from flower.utils.template import humanize
#
# def format_task(task):
#     task.args = humanize(task.args, length=10)
#     task.kwargs.pop('credit_card_number')
#     task.result = humanize(task.result, length=20)
#     return task


