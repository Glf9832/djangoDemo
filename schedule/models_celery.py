from sqlalchemy import Column, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Integer,JSON,DateTime,Float

from common.db_engine import get_engine

Base = declarative_base()
metadata = Base.metadata

class CeleryTaskResult(Base):
    __tablename__ = 'celery_task_result'

    id_task_uuid = Column(String(250),primary_key=True)
    task_name = Column(String(150))
    status = Column(String(40))
    task_args = Column(String(250))
    task_result = Column(JSON)
    task_started = Column(DateTime(True))
    task_succeeded = Column(DateTime(True))
    task_failure = Column(DateTime(True))
    task_end = Column(DateTime(True))
    task_except = Column(String(250))

if __name__ == '__main__':
    engine = get_engine()
    metadata.create_all(engine)
    pass