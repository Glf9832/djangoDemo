# -*- coding:utf-8 -*-

from __future__ import absolute_import
from celery import Celery
# from celery.signals import after_task_publish

celery_app = Celery('schedule',include=['schedule.celery_tasks'])
celery_app.config_from_object('schedule.celery_config')

# app.autodiscover_tasks()

if __name__ == '__main__':
    celery_app.start()
