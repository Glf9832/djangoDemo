# -*- coding:utf-8 -*-

from datetime import timedelta
from celery.schedules import crontab
import os
from common.log import BASE_DIR
from common.config_helper import Redis_Conf,Rabbitmq_Conf

rd = Redis_Conf()
rb = Rabbitmq_Conf()

BROKER_URL = 'amqp://{0}:{1}@{2}:{3}/'.format(rb.user,rb.password,rb.host,rb.port)  #使用RabbitMQ作为消息代理
# BROKER_URL = 'redis://10.110.139.128:6379/1'
CELERY_RESULT_BACKEND = 'redis://{0}:{1}/1'.format(rd.host,rd.port)  #把任务结果存在Redis  'redis://192.168.2.125:6380/1'
CELERY_TASK_SERIALIZER = 'msgpack' #任务序列化和反序列化使用msgpack方案
CELERY_RESULT_SERIALIZER = 'json'   #读取任务结果一般性能要求不高，所以使用了可读性更好的JSON
CELERY_TASK_RESULT_EXPIRES = 60*60     #任务过期时间，不建议直接写86400，应该让这样的magic数字表述更明显 设置成None或0则永不过期
CELERY_ACCEPT_CONTENT = ['json','msgpack']  #指定接受的内容类型

CELERY_TIMEZONE = 'Asia/Shanghai'
CELERY_ENABLE_UTC = True

# crontab()     每分钟执行一次
# crontab(minute=0, hour=0)     每天凌晨十二点执行
# crontab(minute='*/15')        每十五分钟执行一次
# crontab(minute='*',hour='*', day_of_week='sun')   每周日的每一分钟执行一次
# crontab(minute='*/10',hour='3,17,22', day_of_week='thu,fri')  每周三，五的三点，七点和二十二点没十分钟执行一次

LOG_BASE_DIR = os.path.join(BASE_DIR,'log')

CELERYBEAT_SCHEDULE = {
    'channels_task':{
        'task': 'schedule.celery_tasks.channels_test_task',
        'schedule': timedelta(seconds=1),
        # 'schedule':crontab(minute='*/15'),
    },
    'log_check_task':{
        'task': 'schedule.celery_tasks.log_check_task',
        'schedule': crontab(minute='*/30'),
        'args':(
            {'target_dir':LOG_BASE_DIR,'file_types':['.log'],'max_size':100},
        ),
    },
    # 'test_task': {
    #     'task': 'schedule.celery_tasks.test_task',
    #     'schedule': timedelta(seconds=10),
    #     'args':({"hello":"world"},),
    #
    # },
}




