import configparser
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
config_path = os.path.join(BASE_DIR,'conf.ini')

class DB_Base(object):
    def __init__(self):
        self.conf = configparser.ConfigParser()
        self.conf.read(config_path)
        self.dbname = None

    @property
    def host(self):
        return self.conf.get(self.dbname,'host')

    @property
    def port(self):
        return self.conf.get(self.dbname,'port')

    @property
    def user(self):
        return self.conf.get(self.dbname, 'user')

    @property
    def password(self):
        return self.conf.get(self.dbname, 'password')

class Postgresql_Conf(DB_Base):
    def __init__(self):
        super().__init__()
        self.dbname = "postgresql"

    @property
    def db(self):
        return self.conf.get(self.dbname, 'db')

class Mongodb_Conf(DB_Base):
    def __init__(self):
        super().__init__()
        self.dbname = "mongodb"

class Redis_Conf(DB_Base):
    def __init__(self):
        super().__init__()
        self.dbname = "redis"

class Rabbitmq_Conf(DB_Base):
    def __init__(self):
        super().__init__()
        self.dbname = "rabbitmq"


class Scrapyd_Conf(object):
    def __init__(self):
        self.conf = configparser.ConfigParser()
        self.conf.read(config_path)
        self.conf_title = 'scrapyd'

    @property
    def host(self):
        return self.conf.get(self.conf_title,'host')

    @property
    def port(self):
        return self.conf.get(self.conf_title,'port')
