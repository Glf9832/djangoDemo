import subprocess
import re
import sys

def run_cmd(cmd,show_runcmd=True,show_result=True,is_raise_error=True):
    '''run command line'''
    child = subprocess.Popen(
        cmd,shell=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    out,err = child.communicate()
    rc = child.returncode
    result = None
    if rc != 0:
        if is_raise_error:
            # raise RunCommandError('Exec cmd failed : %s'%cmd)
            raise Exception
        else:
            return 'error'
    else:
        if sys.version_info.major == 3:
            result = str(out,encoding='utf-8')
        else:
            result = str(out)
    if show_runcmd:
        if show_result:
            print('==> %s\n\n%s' % (cmd, result))
        else:
            print('==> %s\n' % (cmd))
    return result

def input_select(info,select_ok,select_no):
    if sys.version_info.major == 3:
        input_value = input(info).lower()
    else:
        input_value = raw_input(info).lower()
    if input_value == select_ok.lower():
        return True
    elif input_value == select_no.lower():
        print("exit.")
        return False
    else:
        return input_select("Input error value,please re-enter '%s' or '%s' : "%(select_ok,select_no),select_ok,select_no)

def select_list_print(title,select_list):
    results = [title]
    for i in range(0,len(select_list)):
        results.append('  [%s] %s'%(i+1,select_list[i]))
    if len(select_list)>0:
        results.append('  [*] ALL\n')
        for result in results:
            print(result)
        return results
    return None


def input_selects(info,selects):
    error_info = "Input error value,Please enter select number : "

    if sys.version_info.major == 3:
        input_value = input(info).lower()
    else:
        input_value = raw_input(info).lower()

    if input_value == 'q':
        print("exit.")
        return 'q'
    elif input_value == '*':
        return '*'

    if input_value in selects:
        return input_value
    else:
        try:
            li = input_value.split(',')
            if isinstance(li,list):
                for i in li:
                    if not i.isdigit():
                        return input_selects(error_info,selects)
                return li
            else:
                return input_selects(error_info,selects)
        except:
            return input_selects(error_info, selects)

def select_item(info,items_value):
    selects = [str(j + 1) for j in range(0, len(items_value))]
    error_info = "Input error value,Please enter select number : "
    input = input_selects(info,selects)
    qualified_values = []
    l = len(items_value)
    for i in range(1,l+1):
        qualified_values.append(str(i))
    # if input == '*':
    #     items_value = items_value
    if isinstance(input,str):
        if input.isdigit():
            items_value = [items_value[int(input)-1]]
        elif input == '*':
            items_value = items_value
        elif input == 'q':
            return None
    elif isinstance(input,list):
        is_exist = not [False for i in input if i not in qualified_values]
        if is_exist:
            items_value = [items_value[int(i)-1] for i in input]
        else:
            return select_item(error_info,items_value)
    # elif input == "q":
    #     return None
    else:
        return select_item(error_info,items_value)
    return items_value

def format_result(output_result,filter=5):
    disk_list_infos = []
    for info in output_result:
        info = info.replace(' ','/') + '/'
        new_info = re.findall(r'/*(\S.*?)/',info)
        l = len(new_info)
        if l != 0:
            if len(new_info)>filter:
                for i in range(0,l-filter):
                    new_info[filter-1] = new_info[filter-1]+' '+new_info[filter+i]
            disk_list_infos.append(new_info)
    del disk_list_infos[0]
    return disk_list_infos

class RunCommandError(Exception):
    pass

class InputError(Exception):
    pass

if __name__ == '__main__':
    # cmd1 = 'pwd'
    # result = run_cmd(cmd1)
    # print(result)
    # if 'command_line' in result:
    #     invalue = input('enter cmd:')
    #     print(type(invalue))
    #     if invalue == '1':
    #         print('success')

    # run_cmd('pwd')
    # input_select("rescan is complete !\nClick the 'e' continue or 'q' quit: ", 'e', 'q')

    cmd2 = 'vxdisk list'
    result2 = run_cmd(cmd2)
    # print('==> %s\n%s' % (cmd2, result2))

    disk_list_infos = format_result(result2.split('\n'))

    for dli in disk_list_infos:
        print(dli)
