import psutil
import time

def get_sys_info():
    t = time.strftime('%H:%M:%S', time.localtime())
    cpus = psutil.cpu_percent(interval=None, percpu=True)
    memory = psutil.virtual_memory().percent
    return t,cpus,memory
