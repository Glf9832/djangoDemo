import logging
from datetime import datetime
import os
from os.path import join, getsize
import zipfile
import shutil
from log4mongo.handlers import BufferedMongoHandler
from common.config_helper import Mongodb_Conf

_DEBUG_LOGGER = logging.getLogger('debug')

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
LOG_BASE_DIR = os.path.join(BASE_DIR,'log')

mongo = Mongodb_Conf()

# def cur_date():
#     '''get year and month'''
#     d = datetime.now()
#     return '%4d%02d'%(d.year,d.month)
#
def cur_day():
    '''get year month day'''
    d = datetime.now()
    return '%4d%02d%02d'%(d.year,d.month,d.day)

def cur_timestamp():
    '''get year month、day、hour、minute、second、second'''
    d = datetime.now()
    return '%4d%02d%02d%02d%02d%02d'%(d.year,d.month,d.day,d.hour,d.minute,d.second)

def get_logger():
    global _DEBUG_LOGGER
    return _DEBUG_LOGGER

def get_filename(root,sub_log_folder,log_name='debug'):
    file_fields = [log_name]
    file_fields.append(cur_day())

    filename = '_'.join(file_fields)+'.log'

    log_dir_path = os.path.join(root,'log',sub_log_folder,)

    if not os.path.exists(log_dir_path):
        os.makedirs(log_dir_path,exist_ok=True)

    return os.path.join(root,log_dir_path,filename)

def log_setup(root,sub_log_folder,log_name,level=logging.DEBUG,console=True):
    global _DEBUG_LOGGER

    filename = get_filename(root,sub_log_folder,log_name)

    file_handler = logging.FileHandler(filename,mode='a',encoding='utf-8',delay=False)
    console_handler = logging.StreamHandler()

    log_format = '%(levelname)s %(asctime)s %(module)s %(process)d %(message)s'
    debug_formatter = logging.Formatter(log_format)
    file_handler.setFormatter(debug_formatter)

    if not level:
        _DEBUG_LOGGER.setLevel(logging.DEBUG)
    else:
        _DEBUG_LOGGER.setLevel(level)

    _DEBUG_LOGGER.handlers = []
    _DEBUG_LOGGER.addHandler(file_handler)

    if console:
        _DEBUG_LOGGER.addHandler(console_handler)

def log_setup_db(buffer_size=1,level=logging.WARNING):
    global _DEBUG_LOGGER

    mongodb_handler = BufferedMongoHandler(host=mongo.host,  # All MongoHandler parameters are valid
                                   capped=True,
                                   buffer_size=buffer_size,  # buffer size.
                                   buffer_periodical_flush_timing=10.0,  # periodical flush every 10 seconds
                                   buffer_early_flush_level=logging.CRITICAL,# early flush level
                                   level=level,
                                   )
    _DEBUG_LOGGER.addHandler(mongodb_handler)

def get_file_size(filename,unit='Mb'):
    '''
    获取文件大小
    :param filename:文件名
    :param unit:单位
    :return:
    '''
    statinfo = os.stat(filename)
    if unit == 'Mb':
        return round(statinfo.st_size / 1024/1024, 2)
    elif unit == 'Kb':
        return round(statinfo.st_size/1024,2)

def get_dir_size(dir,unit='Mb'):
    '''
    获取文件夹大小
    :param dir:
    :param unit:
    :return:
    '''
    size = 0
    for root, dirs, files in os.walk(dir):
        size += sum([getsize(join(root, name)) for name in files])
    if unit == 'Mb':
        return round(size/1024/1024,2)
    elif unit == 'Kb':
        return round(size/1024,2)

def get_dirfiles_size(dir,file_types,unit='Mb',is_one_level=True):
    '''
    获取文件内指定类型的文件大小
    :param dir:
    :param file_types:
    :param unit:
    :param is_one_level:
    :return:
    '''
    size = 0
    filenames = []

    for root,dirs,files in os.walk(dir):
        for name in files:
            if file_types[0] == '*':
                size += getsize(join(root,name))
                filenames.append(name)
            else:
                extension = os.path.splitext(name)[1]
                if extension in file_types:
                    size += getsize(join(root,name))
                    filenames.append(name)
        if is_one_level:
            break
    if unit == 'Mb':
        sizes = round(size / 1024 / 1024, 2)
        return {'files':filenames,'sizes':sizes}
    elif unit == 'Kb':
        sizes = round(size / 1024, 2)
        return {'files': filenames, 'sizes': sizes}

def get_subdir_list(dir):
    '''
    获取文件夹内子文件夹列表
    :param dir:
    :return:
    '''
    rootlist = []
    for root,dirs,files in os.walk(dir):
        if root != dir:
            rootlist.append(root)
    return rootlist

def zipdir(zipfilename,startdir,is_deldir=False):
    '''
    压缩文件夹
    :param zipfilename:
    :param startdir:
    :param is_deldir:
    :return:
    '''
    f = zipfile.ZipFile(zipfilename,'w',zipfile.ZIP_DEFLATED)
    for dirpath, dirnames, filenames in os.walk(startdir):
      for filename in filenames:
        f.write(os.path.join(dirpath,filename))
    f.close()

    if is_deldir:
        shutil.rmtree(startdir)

def check_log_dir(log_base_dir,unit='Mb',subdir_max_size=100,is_deldir=False):
    '''
    检查指定文件夹大小,超过阈值则压缩到log根目录
    :param log_base_dir:
    :param unit:
    :param subdir_max_size:
    :param is_deldir:
    :return:
    '''
    zip_dirs = {}
    for sub_log_folder in get_subdir_list(log_base_dir):
        if get_dir_size(sub_log_folder,unit=unit)>=subdir_max_size:
            zipfilename = '%s_log_%s.zip'%(sub_log_folder,cur_timestamp())
            zipdir(zipfilename,sub_log_folder,is_deldir=is_deldir)
            zip_dirs[sub_log_folder] = zipfilename
    return zip_dirs

def check_dir(sub_dir,zip_name,**kwargs):
    '''
    检查指定文件夹下指定类型的文件总大小，超过阈值则压缩到指定位置
    :param targe_dir:
    :param zip_name:
    :param check_file_types:
    :param max_size:
    :param is_deldir:
    :return:
    '''
    # targe_dir = kwargs.get("target_dir")
    filesizes = get_dirfiles_size(sub_dir, kwargs.get("file_types",['.log']), unit=kwargs.get('unit','Mb'))
    if filesizes['sizes'] >= kwargs.get('max_size',100):
        if len(filesizes['files'])==0:
            return
        azip = zipfile.ZipFile(zip_name,'w')
        for filename in filesizes['files']:
            filepath = os.path.join(sub_dir, filename)
            azip.write(filepath, compress_type=zipfile.ZIP_STORED)
            if kwargs.get('is_deldir',True):
                os.remove(filepath)
        azip.close()
        return sub_dir
    return None

def traverse_check_dir(**kwargs):
    '''
    遍历检查文件目录
    :param target_dir:
    :param file_types:
    :param kwargs:
    :return:
    '''
    paths = []
    target_dir = kwargs.get("target_dir")
    paths.append(target_dir)
    for root,dirs,files in os.walk(target_dir):
        for d in dirs:
            paths.append(os.path.join(root,d))
    exec_dirs = []
    for sub_dir in paths:
        zip_name = os.path.join(sub_dir,'%s_log_%s.zip' % (os.path.basename(sub_dir), cur_timestamp()))
        check_result = check_dir(sub_dir,zip_name,**kwargs)
        if check_result is not None:
            exec_dirs.append(check_result)
    return exec_dirs

if __name__ == '__main__':
    LOG_BASE_DIR = os.path.join(BASE_DIR,'log')
    # dirs = check_log_dir(LOG_BASE_DIR,subdir_max_size=0)
    # print(dirs)

    # targe_dir = os.path.join(LOG_BASE_DIR, 'user_id')
    # zip_name = os.path.join(targe_dir,'%s_log_%s.zip' % (os.path.basename(targe_dir), cur_timestamp()))
    # check_dir(targe_dir,zip_name,['.log'],max_size=0,is_deldir=True)

    # paths = []
    # paths.append(LOG_BASE_DIR)
    # for root,dirs,files in os.walk(LOG_BASE_DIR):
    #     for d in dirs:
    #         paths.append(os.path.join(root,d))
    #
    # for d in paths:
    #     zip_name = os.path.join(d,'%s_log_%s.zip' % (os.path.basename(d), cur_timestamp()))
    #     check_dir(d,zip_name,['.log'],max_size=0,is_deldir=True)

    result = traverse_check_dir(target_dir=LOG_BASE_DIR, file_types=['*'],max_size=0,unit='Mb',is_deldir=True)
    print(result)
