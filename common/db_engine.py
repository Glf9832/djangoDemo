from sqlalchemy import Column, String, create_engine
from sqlalchemy.orm import sessionmaker
from common.config_helper import Postgresql_Conf

pg = Postgresql_Conf()

db_uri = 'postgresql+psycopg2://{user}:{password}@{host}:{port}/{db}'.format(
    user = pg.user,
    password = pg.password,
    host = pg.host,
    port = pg.port,
    db = pg.db
)

def get_engine():
    engine = create_engine(db_uri)
    return engine

def get_session():
    engine = create_engine(db_uri)
    session = sessionmaker(bind=engine)
    return session()

if __name__ == '__main__':
    # dbsession = get_session(db_uri)

    # dbsession.add(result)
    # dbsession.commit()
    # dbsession.close()

    # engine = create_engine(db_uri)
    # DBSession = sessionmaker(bind=engine)
    #
    # metadata.create_all(engine)

    # session = DBSession()
    #     # result = CeleryTaskResult(
    #     #     task_name="schedule.celery_tasks.channels_test_task",
    #     #     id_task_uuid = "77e742ba-1b47-4fdd-8e83-a73831951568",
    #     #     status = "successful",
    #     #     task_clock = 19867536,
    #     # )
    #     # session.add(result)
    #     # session.commit()
    #     # session.close()

    pass
