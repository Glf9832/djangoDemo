
from django.conf.urls import url,include
from . import views
from rest_framework import routers
from .views import UserViewSet

router = routers.DefaultRouter()
router.register(r'users',UserViewSet)

urlpatterns = [
    url(r'^$',views.index,name='index'),

    url(r'^api/',include(router.urls)),
    url(r'^api-auth/',include('rest_framework.urls',namespace='rest_framework')),
]

