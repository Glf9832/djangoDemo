
#### 一、Demo启动流程

```
1) #XXX docker run -it --name django-celery-flower -d -p 8001:8001 -p 5555:5555 -p 2203:22 -v /etc/localtime:/etc/localtime:ro django-celery:demo bash
2) celery -B -A schedule.app worker -l info -s ./tmp/celerybeat-schedule
3) flower --address=0.0.0.0 --port=5555 --broker=redis://192.168.2.132:6380/0 --broker_api=redis://192.168.2.132:6380/0
   或者 flower --broker=amqp://guest:guest@10.110.139.128:5672/ --conf=schedule/flowerconfig.py
4) python manage runserver 0.0.0.0:8001
5) 浏览器http://localhost:5555查看flower
```

#### 二、Celery配置

###### 2.1 基本配置
```
BROKER_URL = 'amqp://guest:guest@192.168.2.129:5676//'  #使用RabbitMQ作为消息代理
BROKER_URL = 'redis://192.168.2.132:6380/0'
CELERY_RESULT_BACKEND = 'redis://192.168.2.132:6380/1'  #把任务结果存在Redis  'redis://192.168.2.125:6380/1'
CELERY_TASK_SERIALIZER = 'msgpack' #任务序列化和反序列化使用msgpack方案
CELERY_RESULT_SERIALIZER = 'json'   #读取任务结果一般性能要求不高，所以使用了可读性更好的JSON
CELERY_TASK_RESULT_EXPIRES = 60     #任务过期时间，不建议直接写86400，应该让这样的magic数字表述更明显 设置成None或0则永不过期
CELERY_ACCEPT_CONTENT = ['json','msgpack']  #指定接受的内容类型
```

###### 2.2 定时调度的时间设置
```
crontab()     每分钟执行一次
crontab(minute=0, hour=0)     每天凌晨十二点执行
crontab(minute='*/15')        每十五分钟执行一次
crontab(minute='*',hour='*', day_of_week='sun')   每周日的每一分钟执行一次
crontab(minute='*/10',hour='3,17,22', day_of_week='thu,fri')  每周三，五的三点，七点和二十二点没十分钟执行一次
```

###### 2.3 worker管理
```
celery multi start djangopro -B -A schedule.celery_app -l info --pidfile=/home/glf/djangoDemo/tmp/celery_%h.pid --logfile=/home/glf/djangoDemo/tmp/celery_%h.log

celery multi show djangopro
celery multi names djangopro
celery multi stop djangopro
celery multi restart djangopro
celery multi kill djangopro

```
